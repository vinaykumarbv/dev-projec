package com.hungerbox;

import org.springframework.batch.core.configuration.annotation.EnableBatchProcessing;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.cloud.netflix.ribbon.RibbonClient;
import org.springframework.cloud.openfeign.EnableFeignClients;

import com.hungerbox.config.RibbonConfiguration;

@SpringBootApplication
@EnableFeignClients
@EnableEurekaClient
//@EnableScheduling
@EnableBatchProcessing
@RibbonClient(value = "order-loadbalance", configuration = RibbonConfiguration.class)
public class HungerBoxAppApplication implements CommandLineRunner {

	public static void main(String[] args) {
		SpringApplication.run(HungerBoxAppApplication.class, args);
	}

	@Override
	public void run(String... args) throws Exception {
		

	}

}
