package com.hungerbox.batch;

import java.util.Optional;

import org.springframework.batch.item.ItemProcessor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.hungerbox.entity.FoodMenu;
import com.hungerbox.repository.FoodMenuRepository;

@Component
public class Processor implements ItemProcessor<FoodMenu, FoodMenu> {

	@Autowired
	private FoodMenuRepository foodMenuRepository;

	@Override
	public FoodMenu process(FoodMenu foodMenu) throws Exception {
		Optional<FoodMenu> foodMenuFromDb = foodMenuRepository.findById(foodMenu.getFoodId());
		if (foodMenuFromDb.isPresent()) {
			foodMenu.setFoodName(foodMenu.getFoodName());
		}
		return foodMenu;
	}

}
