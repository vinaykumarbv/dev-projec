package com.hungerbox.batch;

import java.util.List;

import org.springframework.batch.item.ItemWriter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.hungerbox.entity.FoodMenu;
import com.hungerbox.repository.FoodMenuRepository;

@Component
public class Writer implements ItemWriter<FoodMenu> {

	@Autowired
	private FoodMenuRepository foodMenuRepository;

	@Override
	@Transactional
	public void write(List<? extends FoodMenu> items) throws Exception {
		foodMenuRepository.saveAll(items);
	}

}