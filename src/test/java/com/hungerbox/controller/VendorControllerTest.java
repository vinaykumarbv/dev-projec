package com.hungerbox.controller;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;

import java.util.List;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import com.hungerbox.dto.VendorDto;
import com.hungerbox.entity.FoodMenu;
import com.hungerbox.service.VendorService;

@ExtendWith(MockitoExtension.class)
public class VendorControllerTest {

	@Mock
	VendorService vendorService;

	@InjectMocks
	VendorController vendorController;

	static VendorDto vendorDto;

	static FoodMenu foodMenu1;
	static FoodMenu foodMenu2;

	@BeforeAll
	public static void setup() {
		vendorDto = new VendorDto();
		foodMenu1  = new FoodMenu();
		foodMenu2  = new FoodMenu();
		vendorDto.setVendorId(123);
		vendorDto.setVendorName("dosatalks");
		vendorDto.setVendorLocation("banglore");
		vendorDto.setVendorPhoneNo("12345");
		vendorDto.setVendorEmail("vin@gmail.com");
		foodMenu1.setFoodId(111);
		foodMenu1.setFoodName("dosa");
		foodMenu1.setFoodPrice(40);
		foodMenu1.setFoodDescription("veg");
		vendorDto.getFoodMenu().add(foodMenu1);
		vendorDto.getFoodMenu().add(foodMenu2);
		
	}
	
		@Test
		public void vendorTest() {
			
			String vendorName = "dosatalks";
			int pageNumber=0;
			int pageSize =10;
			
			when(vendorService.findByvendorNameContains(vendorName, pageNumber, pageSize)).thenReturn((List<VendorDto>) vendorDto);
			
			
			//assertEquals(expected, actual);
		}
		
		
		
		
	}


